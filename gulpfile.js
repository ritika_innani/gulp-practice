var gulp = require("gulp");
var ts = require("gulp-typescript");
var sourcemaps = require('gulp-sourcemaps');
var tsProject = ts.createProject("tsconfig.json");
var merge = require('merge2');

gulp.task("ts", function () {
    var tsResult = gulp.src(["main.ts"], {base: '.'})
        .pipe(sourcemaps.init())
        .pipe(tsProject());

    return merge([
        tsResult.dts.pipe(gulp.dest('.')),
        tsResult.js.pipe(sourcemaps.write('.')).pipe(gulp.dest('.')),
        tsResult.pipe(sourcemaps.write('.'))
    ]);
});

gulp.exports = ts;
